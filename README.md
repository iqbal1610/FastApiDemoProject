
## local installation with docker 

A dockerfile for demo app added. A `docker-compose.yml` file consists of  service `app`  added.

To run, execute the following command:
   ```
   docker-compose up
   ```
   (It will clean up existing containers and force to be recreated)
 
To test your API you can check [here](http://127.0.0.1:80/docs) on your browser.

## Kubernetes installation with kustomize
Make sure you have installed docker,kubernetes(minikube or docker desktop kubernetes),kubectl and kustomize

The docker image is available [here](https://hub.docker.com/repository/docker/iqbalict/fastapi-demo) 

You can configure/setup the demo-app both in dev and prod environment via kustomize.

### Setup dev environment 

```
kustomize build kustomize/overlays/dev
kubectl apply -k kustomize/overlays/dev
```
Check pod is up and running

```
kubectl get pods -n dev
```
Port forward your service to access the application

```
kubectl port-forward svc/demo-app 8888:80 -n dev
```
Now you can access the service from [here](http://127.0.0.1:8888/docs) 


### Setup prod environment 

```
kustomize build kustomize/overlays/prod
kubectl apply -k kustomize/overlays/prod
```
Check pod is up and running

```
kubectl get pods -n prod
```
Port forward your service to access the application

```
kubectl port-forward svc/demo-app 8881:80 -n prod
```
Now you can access the service from [here](http://127.0.0.1:8881/docs) 
