import os

from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from dotenv import load_dotenv

if not os.getenv("SQLITE_PATH"):
    load_dotenv()

# Create a sqlite engine instance
SQLITE_PATH = os.getenv("SQLITE_PATH")
engine = create_engine(SQLITE_PATH)

# Create a DeclarativeMeta instance
Base = declarative_base()

# Create SessionLocal class from sessionmaker factory
SessionLocal = sessionmaker(bind=engine, expire_on_commit=False)
